/*
 * 请告诉我所有被取消（'Cancelled'）的订单以及其总金额。查询结果应当包含如下的内容：
 *
 * +──────────────+─────────────+───────────────+
 * | orderNumber  | totalPrice  | detailsCount  |
 * +──────────────+─────────────+───────────────+
 *
 * 其中，orderNumber 是订单编号，totalPrice 是订单的总金额而 detailsCount 是每一个订单
 * 包含的 `orderdetails` 的数目。
 *
 * 结果应当按照 `orderNumber` 排序。
 */

select
  o.`orderNumber`,
  sum(d.`priceEach` * d.`quantityOrdered`) as totalPrice,
  count(d.`orderNumber`) as detailsCount
from orders as o
  left join orderdetails as d on o.`orderNumber` = d.`orderNumber`
where o.status='Cancelled'
group by `orderNumber`;
