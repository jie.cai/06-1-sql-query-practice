/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */

select
  min(t.orderDetailCount) as minOrderItemCount,
  max(t.orderDetailCount) as maxOrderItemCount,
  round(avg(t.orderDetailCount)) as avgOrderItemCount
from (
  select
    count(d.`orderNumber`) as orderDetailCount
  from orders as o
    left join orderdetails as d on o.`orderNumber` = d.`orderNumber`
  group by o.orderNumber
  order by orderDetailCount
) as t;
