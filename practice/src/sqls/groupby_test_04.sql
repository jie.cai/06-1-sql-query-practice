/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */

select
  d.`orderNumber`,
  sum(d.`priceEach` * d.`quantityOrdered`) as totalPrice
from orders as o
  left join orderdetails as d on o.`orderNumber` = d.`orderNumber`
group by orderNumber
having totalPrice > 60000
order by totalPrice desc;
