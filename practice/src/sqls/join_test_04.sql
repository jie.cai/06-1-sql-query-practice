/*
 * 请告诉我顾客（customer）中顾客编号（customerNumber）小于 150 的顾客每一个人的订单（order）
 * 数目。 查询结果中应当包含如下的信息：
 *
 * +─────────────────+──────────────+
 * | customerNumber  | totalOrders  |
 * +─────────────────+──────────────+
 *
 * 查询结果请按照 `customerNumber` 排序。
 */

select
  c.`customerNumber`,
  count(o.`orderNumber`) as totalOrders
from customers as c
  left join orders as o on c.`customerNumber` = o.`customerNumber`
where c.`customerNumber` < 150
group by c.`customerNumber`;
