/*
 * 请告诉我所有员工（employee）中有管理者（manager）的员工姓名及其管理者的姓名。所有的姓名
 * 请按照 `lastName, firstName` 的格式输出。例如：`Bow, Anthony`：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */

select
    concat(e.`lastName`, ', ', e.`firstName`) as employee,
    if(isnull(e.`reportsTo`), null, concat(ex.`lastName`, ', ', ex.`firstName`)) as manager
from employees as e
  left join employees as ex on e.`reportsTo` = ex.`employeeNumber`
having not isnull(manager)
order by manager, employee;
