/*
 * 请告诉我所有的订单（`order`）中每一种 `status` 的订单的总金额到底是多少。注意是总金额哦。输出
 * 应当包含如下的信息：
 *
 * +─────────+─────────────+
 * | status  | totalPrice  |
 * +─────────+─────────────+
 *
 * 输出应当根据 `status` 进行排序。
 */

select
  status,
  sum(d.`priceEach` * d.`quantityOrdered`) as totalPrice
from orders as o
  left join orderdetails as d on o.`orderNumber` = d.`orderNumber`
group by status;
