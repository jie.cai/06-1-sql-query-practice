/*
 * 请告诉我那些没有下任何订单的顾客（customer）的信息。结果应当包含如下的信息：
 *
 * +─────────────────+───────────────+
 * | customerNumber  | customerName  |
 * +─────────────────+───────────────+
 *
 * 结果应当按照 `customerNumber` 排序。
 */

select c.`customerNumber`, c.`customerName` from (
  select
    c.`customerNumber` as customerNumber
  from customers as c
    left join orders as o on c.`customerNumber` = o.`customerNumber`
  group by c.customerNumber
  having count(o.`customerNumber`)=0
) as t
  left join customers as c on c.`customerNumber` = t.customerNumber;
